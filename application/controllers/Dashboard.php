<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		flash_msg('Records Save Successfully','success');
		$data = array();
		Admin(array(
			'title' => 'Dashboard - Admin panel',
			'content_view' => 'dashboard',
			'content_data' => $data
		));
	}
	public function lockscreen()
	{
		//flash_msg('Records Save Successfully','success');
		$data = array();
		$data['title'] = "Lockscreen - Admin panel";
		$this->load->view('lockscreen',$data);
	}
}

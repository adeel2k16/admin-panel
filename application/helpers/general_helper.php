<?php
function encode($encode_value)
{
	$CI =& get_instance();
	$encypted_value = $CI->encryption->encrypt($encode_value);
	$encypted_value = str_replace(array('+', '/', '='), array('-', '_', '~'), $encypted_value);
	return $encypted_value;
}
function decode($decode_value)
{
	$CI =& get_instance();
	$decrypt_value= str_replace(array('-', '_', '~'), array('+', '/', '='), $decode_value);
	return $CI->encryption->decrypt($decrypt_value);
}
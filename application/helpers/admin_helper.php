<?php
function Admin($options = array())
{
	$CI =& get_instance();
	
	if(isset($options['title']) && !empty($options['title'])){
	$options['application_data']['title'] = $options['title'];}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	
	$options['application_data']['content'] = $CI->load->view($options['content_view'], 
	$options['content_data'], true);
	
	$CI->load->view('index.php', $options['application_data']);
}

function validation_error($field_name)
{
	
	return '<ul class="parsley-errors-list filled"><li class="parsley-custom-error-message parsley-required">'.form_error($field_name).'</li></ul>';
}

function flash_msg($message = '',$type = '',$mehtod = '',$hideit = FALSE)
{
	$CI =& get_instance();
	
	if($message != ''){
		
		$CI->session->set_flashdata('flash_msg_'.$type, $message);
		
	}
	
	if($CI->session->flashdata('flash_msg_'.$type) != '' 
	&& $CI->session->flashdata('flash_msg_'.$type) != NULL && $mehtod == 'get'){
		
		$is_hideit = '';
		if($hideit == TRUE){
			$is_hideit = 'true';
		}
		else{$is_hideit = 'false';}
		
		if($type  == 'success'){
		$success = '<script type="text/javascript">$(function() {$.jGrowl("'.$CI->session->flashdata('flash_msg_'.$type).'", 
		{sticky: '.$is_hideit.',position: "top-right",theme: "bg-blue-alt"});});</script>';
		unset($_SESSION['flash_msg_'.$type]); 
		return $success;
		}
		
		if($type  == 'danger'){
		
		$error = '<script type="text/javascript">$(function() {$.jGrowl("'.$CI->session->flashdata('flash_msg_'.$type).'", 
		{sticky: '.$is_hideit.',position: "top-right",theme: "bg-red"});});</script>';
		unset($_SESSION['flash_msg_'.$type]);
		return $error;
		}
		
		if($type  == 'warning'){
			
		$warning = '<script type="text/javascript">$(function() {$.jGrowl("'.$CI->session->flashdata('flash_msg_'.$type).'", 
		{sticky: '.$is_hideit.',position: "top-right",theme: "bg-orange"});});</script>';
		unset($_SESSION['flash_msg_'.$type]);
		return $warning;
		}
		
		if($type  == 'information'){
		$information =  '<script type="text/javascript">$(function() {$.jGrowl("'.$CI->session->flashdata('flash_msg_'.$type).'", 
		{sticky: '.$is_hideit.',position: "top-right",theme: "bg-green"});});</script>';
		unset($_SESSION['flash_msg_'.$type]);
		return $information;
		}
	}
}
	

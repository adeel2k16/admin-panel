<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
	function __construct()
 	{
  		parent::__construct();
		$this->encryption->initialize(
		array(
			'driver' => 'mcrypt',
			'cipher' => 'Blowfish',
			'mode' => 'cbc',
			'key' => ENCRYPTION_KEY)
		);
		$this->load_helpers();
	}
	function load_helpers()
	{
		$this->load->helper('general');
		$this->load->helper('admin');
	
	}
}